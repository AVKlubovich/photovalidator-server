# qat-server

##Описание проекта
Система для удалённой проверки машин такси у частных водителей. Серверная часть.

##Используемые технологии
[C++14](https://ru.wikipedia.org/wiki/C%2B%2B14);
[Qt5](https://ru.wikipedia.org/wiki/Qt)

##Системные требования
Windows; Linux
nginx
apache
PostgreSql v9.4 и выше

##Документация
[Соглашения по оформлению кода](https://drive.google.com/open?id=0B48GpktEZIksWjNjTmdWcW53Rnc)

##Список контрибьюторов
[@github/avklubovich](../../../../avklubovich)
[@github/AlexandrDedckov](../../../../AlexandrDedckov)
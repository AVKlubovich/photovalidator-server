<?php

class DeletePos extends BasePos
{
    public function exec(&$assoc)
    {        
	if(!array_key_exists('pos', $assoc))
        {
            Utils::print_data(
                array('status' => Errors::NOT_SEND_FIELD, 
                'error' => Errors::instance()->data(Errors::NOT_SEND_FIELD)));
	}
        $pos = $assoc["pos"];
        
        if(!is_array($assoc) ||
           count($pos) == 0)
        {
            Utils::print_data(
                array('status' => Errors::NOT_SEND_FIELD, 
                'error' => Errors::instance()->data(Errors::NOT_SEND_FIELD)));
	}
        
        $assoc["status"] = 1;
    }
}

?>
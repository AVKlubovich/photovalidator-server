<?php

class EditPhotos extends BasePhotos
{
    public function exec(&$assoc)
    {
        $dir_name = dirname($_SERVER['REQUEST_URI']);
	$dir_name = $dir_name == "/" ? "/" : $dir_name."/";
	$url_files = "http://$_SERVER[SERVER_ADDR]".$dir_name.Config::TARGET_DIR;
        $url_drivers_photos = "http://$_SERVER[SERVER_ADDR]".$dir_name.Config::DRIVER_PHOTOS_DIR;

        $is_send_images = false;
	foreach($_FILES as $name=>$file)
	{
            if(strpos($name, "pos") == 0 &&
                !empty($file["size"]))
            {
                $is_send_images = true;
                break;
            }
	}
        
	if(!$is_send_images ||
            !array_key_exists('driver_id', $assoc) ||
            !array_key_exists('auto_id', $assoc))
        {
            $this->sendErrorToServer($assoc);
            Utils::print_data(
                array('status' => Errors::NOT_SEND_FIELD, 
                'error' => Errors::instance()->data(Errors::NOT_SEND_FIELD)));
	}
        $this->checkOldPhotos($assoc);
        
        $url_photo_driver = $this->selectUrlPhotoDriver($assoc);
        if(empty($url_photo_driver))
        {
            $tmp = $this->downloadPhotoDriver($assoc);
            if(!empty($tmp))
                $url_photo_driver = "$url_drivers_photos$tmp";
        }
        $assoc["url_photo_driver"] = $url_photo_driver;
                
	$images = array();
	foreach($_FILES as $name=>$file)
	{
            if(strpos($name, "pos") != 0 ||
                empty($file["size"]))
		continue;
            $file_name = $this->saveImage(Config::TARGET_DIR, $file, $assoc["driver_id"], $name);
            $image = array();
            $image["url"] = "$url_files$file_name";
            $image["pos"] = substr($name, strlen("pos"));
            array_push($images, $image);
            //echo "<p><a href=\"$url_files$file_name\">$file_name</a></p>";
	}
	$assoc["images"] = $images;
        
        $this->sendOkToServer($assoc);
    }
}

?>
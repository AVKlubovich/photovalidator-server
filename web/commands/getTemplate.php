<?php

class GetTemplate extends BaseCommand
{
    public function exec(&$assoc)
    {
        $data = DBManager::selectData(
            'SELECT * '.
            'FROM public.template_images '.
            'ORDER BY pos', 
            array());
        if(is_string($data))
        {
            Utils::print_data(
                array('status' => $data, 
                'error' => Errors::instance()->data($data)));
        }
        
        $template = array();
        for($i = 0; $i < count($data); $i++)
	{
            $url = $data[$i]['url'];
            if(empty($url))
                continue;
            $pos = strripos($url, '/');
            if(!$pos ||
               strlen($url) == $pos+1)
                continue;
            $data[$i]['url'] = substr($url, $pos+1);
            array_push($template, $data[$i]);
        }
        
        $response = array(
            'type_command'=>Commands::GET_TEMPLATE,
            'template'=>$template,
            'status'=>1
            );
        
        $json = Utils::to_json($response);
        echo $json;
        exit();
    }
}

?>

<?php

class EditPos extends BasePos
{
    public function exec(&$assoc)
    {
        $dir_name = dirname($_SERVER['REQUEST_URI']);
	$dir_name = $dir_name == "/" ? "/" : $dir_name."/";
	$url_files = "http://$_SERVER[SERVER_ADDR]".$dir_name.Config::TARGET_DIR_POS;

        $is_send_images = false;
	foreach($_FILES as $name=>$file)
	{
            if($name == "image" &&
               !empty($file["size"]))
            {
                $is_send_images = true;
                break;
            }
	}
        
	if(!$is_send_images ||
           !array_key_exists('pos', $assoc) ||
           !array_key_exists('comment', $assoc))
        {
            Utils::print_data(
                array('status' => Errors::NOT_SEND_FIELD, 
                'error' => Errors::instance()->data(Errors::NOT_SEND_FIELD)));
	}
                
	foreach($_FILES as $name=>$file)
	{
            if($name != "image" ||
                empty($file["size"]))
		continue;
            
            $file_name = $this->saveImage(Config::TARGET_DIR_POS, $file);
            $assoc["url"] = "$url_files$file_name";
            //echo "<p><a href=\"$url_files$file_name\">$file_name</a></p>";
            break;
	}
        $assoc["status"] = 1;
    }
}

?>
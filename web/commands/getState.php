<?php

class GetState extends BaseCommand
{
    public function exec(&$assoc)
    {
        if(!array_key_exists('driver_id', $assoc))
        {
            Utils::print_data(
                array('status' => Errors::NOT_SEND_FIELD, 
                'error' => Errors::instance()->data(Errors::NOT_SEND_FIELD)));
	}
        
        if(!array_key_exists('type_command', $assoc))
            $assoc["type_command"] = Commands::GET_STATE;
        
        $result = NetworkManager::sendJson(Config::HOST_CPP, $assoc);
        if($result == Errors::NOT_SEND_DATA_TO_REMORE_SERVER)
        {
            Utils::print_data(
                array('status' => $result, 
                'error' => Errors::instance()->data($result)));
        }

        echo $result;
        exit();
    }
}

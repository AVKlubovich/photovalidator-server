<?php

class BasePos extends BaseCommand
{
    protected function get_name_file()
    {
        $date = date_create();
	return date_format($date, 'Y-m-d_H-i-s-').substr((string)microtime(), 2, 7);
    }

    protected function saveImage($folder, &$imageInfo)
    {
        $file_name = 'pos_'.$this->get_name_file().substr($imageInfo["name"] , strrpos($imageInfo["name"], ".", -1));
	if (move_uploaded_file($imageInfo["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/$folder$file_name"))
            return $file_name;
        else {
            Utils::print_data(
                array('status' => Errors::NOT_SAVE_FILE, 
                'error' => Errors::instance()->data(Errors::NOT_SAVE_FILE)));
	}
    }
    
    public function exec(&$assoc)
    { }
}

?>
<?php

class Commands
{
    const SET_PHOTOS = "set_photos";
    const EDIT_PHOTOS = "edit_photos";
    const ADD_POS = "add_pos";
    const EDIT_POS = "edit_pos";
    const DELETE_POS = "delete_pos";
    const GET_TEMPLATE = "get_template";
    const GET_STATE = "get_state";
}

?>
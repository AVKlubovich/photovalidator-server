<?php
/*echo '<form action="" method="post" enctype="multipart/form-data"> 
 <p>type_command: <input type="text" name="type_command"></p>
 <p>json: <input type="text" name="json"></p>
 <p>Pos0: <input type="file" name="image"></p>
 <p>Pos1: <input type="file" name="pos1"></p>
 <p>Pos2: <input type="file" name="pos2"></p>
 <p>Pos3: <input type="file" name="pos3"></p>
 <p>Pos4: <input type="file" name="pos4"></p>
 <p>Pos5: <input type="file" name="pos5"></p>
 <p><input type="submit" value="sendData"></p> ';*/

require_once 'errors.php';
require_once 'config.php';
require_once 'utils.php';
require_once 'networkManager.php';
require_once 'dbManager.php';
require_once 'commands.php';

require_once './commands/base/baseCommand.php';
require_once './commands/base/basePhotos.php';
require_once './commands/base/basePos.php';

require_once './commands/editPhotos.php';
require_once './commands/setPhotos.php';
require_once './commands/addPos.php';
require_once './commands/editPos.php';
require_once './commands/deletePos.php';
require_once './commands/getTemplate.php';
require_once './commands/getState.php';

class Query
{
//{ "driver_full_name":"test", "driver_phone_number":"1111111", "driver_id":"11", "auto_color":"yelow", "auto_model":"309", "auto_marka":"pegot", "auto_number":"1111", "franchising_name":"", "auto_id":"1" }
	
    public function run()
    {        
        if(empty($_POST) ||
           !isset($_POST["type_command"]) ||
           !isset($_POST["json"])) 
        {
            Utils::print_data(
                array('status' => Errors::NOT_SEND_FIELD, 
                'error' => Errors::instance()->data(Errors::NOT_SEND_FIELD)));
        }
        $type_command = $_POST["type_command"];
        
        $command = $this->getCommand($type_command);
        if(is_null($command))
        {
            Utils::print_data(
                array('status' => Errors::NOT_SEND_FIELD, 
                'error' => Errors::instance()->data(Errors::NOT_SEND_FIELD)));
        }
        $assoc = json_decode(urldecode($_POST["json"]), true); //$_POST["json"]
        $command->exec($assoc);
        
        if(!array_key_exists('type_command', $assoc))
            $assoc["type_command"] = $type_command;
		
        $result = NetworkManager::sendJson(Config::HOST_CPP, $assoc);
        if($result == Errors::NOT_SEND_DATA_TO_REMORE_SERVER)
        {
            Utils::print_data(
                array('status' => $result, 
                'error' => Errors::instance()->data($result)));
        }
                
        Utils::print_ok();
    }
    
    function getCommand($type_command)
    {
        if(Commands::SET_PHOTOS == $type_command)
            return new SetPhotos();
        elseif(Commands::EDIT_PHOTOS == $type_command) 
            return new EditPhotos();
        elseif(Commands::ADD_POS == $type_command) 
            return new AddPos();
        elseif(Commands::EDIT_POS == $type_command) 
            return new EditPos();
        elseif(Commands::DELETE_POS == $type_command) 
            return new DeletePos();
        elseif(Commands::GET_TEMPLATE == $type_command) 
            return new GetTemplate();
        elseif(Commands::GET_STATE == $type_command) 
            return new GetState();
        else
            return NULL;
    }
}
	
$query = new Query();
$query->run();

?>

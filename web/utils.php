<?php

class Utils
{
    public static function print_data($data)
    {
        print(Utils::to_json($data));
	exit();
    }
    
    public static function print_error($error)
    {
        Utils::print_data(array('status' => -1, 'error' => $error));
    }
    
    public static function print_ok()
    {
	Utils::print_data(array('status' => 1));
    }
    
    public static function to_json($data)
    {
	return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}

?>
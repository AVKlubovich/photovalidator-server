#include "Common.h"
#include "AcceptDriver.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/DefaultResponse.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

#include "web-exchange/WebRequestManager.h"
#include "web-exchange//WebRequest.h"

RegisterCommand(photo_validator_server::AcceptDriver, "accept_driver")


using namespace photo_validator_server;

AcceptDriver::AcceptDriver(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> AcceptDriver::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime();

    auto incomingData = _context._packet.body().toMap()["body"].toMap();

    if (!incomingData.contains("id_driver") ||
        !incomingData.contains("driver_status") ||
        !incomingData.contains("user_login") ||
        !incomingData.contains("user_pass"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto driverId = incomingData["id_driver"].toULongLong();
    const auto driverStatus = incomingData["driver_status"].toUInt();
    const auto& comment = incomingData["comment"].toString();

    auto webManager = network::WebRequestManager::instance();
    auto webRequest = QSharedPointer<network::WebRequest>::create("sub_qry");

    quint8 autoStatus;
    switch (driverStatus)
    {
        case 1:
            autoStatus = 1;
            break;
        case 2:
            autoStatus = 18;
            break;
        case 4:
            autoStatus = 19;
            break;
        default:
            setError(QObject::tr("error: field not sended"));
    }

    QVariantMap userData;
    //type_query=sub_qry&
    //sub_qry=change_auto_status&
    //user_login=****&
    //user_pass=****&
    //name=auto_photo&
    //pass=validator&
    //auto_id=1&
    //auto_status=19
    userData["sub_qry"] = "change_auto_status";
    userData["user_login"] = incomingData["user_login"];
    userData["auto_id"] = QString::number(incomingData["auto_id"].toULongLong());
    userData["auto_status"] = QString::number(autoStatus);
    userData["user_login"] = incomingData["user_login"];
    userData["user_pass"] = QString(QCryptographicHash::hash(incomingData["user_pass"].toString().toStdString().data(),QCryptographicHash::Md5).toHex());

    if (!comment.isEmpty())
    {
        userData["msg_to_driver"] = comment;
    }

    const auto& photosList = incomingData["failed_photos"].toList();
    QStringList photosListStr;
    for (const auto& photoNumber : photosList)
        photosListStr << photoNumber.toString();
    if (!photosListStr.isEmpty())
    {
        userData["failed_photos"] = photosListStr.join(",");
    }

    webRequest->setArguments(userData);
    webRequest->setCallback(nullptr);
    webManager->sendRequestCurrentThread(webRequest);

    const auto& webData = webRequest->reply();
    webRequest->release();

    const auto doc = QJsonDocument::fromJson(webData);
    auto jobj = doc.object();
    const auto map = jobj.toVariantMap();

    if (!map.contains("status"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        setError(qPrintable(QObject::tr("Неверный ответ от удалённого сервера")));
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto status = map.value("status").toInt();
    if(status < 0)
    {
        setError(qPrintable(QObject::tr("Неверный ответ от удалённого сервера")));
        return QSharedPointer<network::Response>();
    }

    if (driverStatus == 2 && !incomingData.contains("failed_photos"))
    {
        qDebug() << __FUNCTION__ << "error: field not sended";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    if (incomingData.contains("group_id"))
    {
        const auto groupId = incomingData["group_id"].toULongLong();
        const auto userId = incomingData["id_user"].toULongLong();

        const auto& acceptDriverStr = QString(
            "UPDATE info_photos "
            "SET "
            "status = :driverStatus, "
            "op_comment = :comment, "
            "id_user = :userId, "
            "date_close = localtimestamp "
            "WHERE id = :groupId"
            );

        const auto wraper = database::DBManager::instance().getDBWraper();
        auto acceptDriverQuery = wraper->query();
        acceptDriverQuery.prepare(acceptDriverStr);
        acceptDriverQuery.bindValue(":driverStatus", driverStatus);
        acceptDriverQuery.bindValue(":comment", comment);
        acceptDriverQuery.bindValue(":userId", userId);
        acceptDriverQuery.bindValue(":groupId", groupId);

        const auto& acceptDriverResult = wraper->execQuery(acceptDriverQuery);
        if (!acceptDriverResult)
        {
            qDebug() << __FUNCTION__ << "error:" << qPrintable(acceptDriverQuery.lastError().text());
            //Q_ASSERT(false);
            return QSharedPointer<network::Response>();
        }

        if (!photosList.isEmpty())
        {
            const auto& setFailedPhotosStr = QString(
                "UPDATE info_photos "
                "SET "
                "failed_photos = :failedPhotos "
                "WHERE id = :groupId"
                );

            auto setFailedPhotosQuery = wraper->query();
            setFailedPhotosQuery.prepare(setFailedPhotosStr);
            setFailedPhotosQuery.bindValue(":failedPhotos", photosListStr.join(","));
            setFailedPhotosQuery.bindValue(":groupId", groupId);

            const auto& setFailedPhotosResult = wraper->execQuery(setFailedPhotosQuery);
            if (!setFailedPhotosResult)
            {
                qDebug() << __FUNCTION__ << "error:" << qPrintable(setFailedPhotosQuery.lastError().text());
                //Q_ASSERT(false);
                return QSharedPointer<network::Response>();
            }
        }
    }

    QVariantMap head;
    QVariantMap body;
    QVariantMap result;

    head["type"] = signature();

    body["status"] = 1;
    body["id_driver"] = driverId;
    body["driver_name"] = incomingData["driver_name"];
    body["date"] = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

void AcceptDriver::setError(const QString& errorStr)
{
    QVariantMap body;
    QVariantMap head;
    QVariantMap result;

    head["type"] = signature();
    body["status"] = -1;
    body["error"] = errorStr;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(result));
}

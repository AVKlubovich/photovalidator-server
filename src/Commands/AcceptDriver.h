#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class AcceptDriver :
            public core::Command,
            public core::CommandCreator<AcceptDriver>
    {
        friend class QSharedPointer<AcceptDriver>;

        AcceptDriver(const Context& newContext);

    public:
        QSharedPointer<network::Response> exec() override;

    private:
        void setError(const QString& errorStr);
    };

}

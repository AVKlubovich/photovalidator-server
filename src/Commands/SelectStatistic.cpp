#include "Common.h"
#include "SelectStatistic.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

#include "permissions/PermissionManager.h"

RegisterCommand(photo_validator_server::SelectStatistic, "select_statistic")


using namespace photo_validator_server;

SelectStatistic::SelectStatistic(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> SelectStatistic::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime();

    const auto& incomingData = _context._packet.body().toMap()["body"].toMap();

    if (!incomingData.contains("date_start") ||
        !incomingData.contains("date_end"))
    {
        qDebug() << __FUNCTION__ << "error: field not sended";
        return QSharedPointer<network::Response>();
    }

    const auto wraper = database::DBManager::instance().getDBWraper();

    const auto& dateStart = incomingData["date_start"].toDateTime().toString("yyyy-MM-dd hh:mm");
    const auto& dateEnd = incomingData["date_end"].toDateTime().toString("yyyy-MM-dd hh:mm");

    const auto queryStr = QString(
        "SELECT DISTINCT statuses.name, COUNT(*) FROM statuses"
        " INNER JOIN info_photos ON (statuses.id = info_photos.status)"
        " WHERE statuses.id <> 0 AND (date_create BETWEEN :dateStart AND :dateEnd)"
        " GROUP BY statuses.name"
        );

    auto query = wraper->query();
    query.prepare(queryStr);
    query.bindValue(":dateStart", dateStart);
    query.bindValue(":dateEnd", dateEnd);

    const auto notAllowedResult = wraper->execQuery(query);
    if (!notAllowedResult)
    {
        qDebug() << __FUNCTION__ << "database error:" << qPrintable(query.lastError().text());
        return QSharedPointer<network::Response>();
    }

    const auto& resultList = database::DBHelpers::queryToVariant(query);

    QVariantMap head;
    head["type"] = signature();

    QVariantMap body;
    body["statistic"] = resultList;
    body["date_start"] = dateStart;
    body["date_end"] = dateEnd;

    QVariantMap result;
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

#include "Common.h"
#include "EditPhotos.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "web-exchange/WebRequestManager.h"
#include "web-exchange/WebRequest.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(photo_validator_server::EditPhotos, "edit_photos")


using namespace photo_validator_server;

EditPhotos::EditPhotos(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> EditPhotos::exec()
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    auto incomingData = _context._packet.body().toMap();

    QMap<QString, QString> mapPos;
    for (int i = 0; i < PHOTOS_COUNT; i++)
    {
        auto key = QString("pos%1").arg(i);
        auto it = incomingData.find(key);
        if (it != incomingData.end())
            mapPos.insert(key, it.value().toString());
    }

    if(!incomingData.contains("driver_id") ||
       !incomingData.contains("driver_full_name") ||
       !incomingData.contains("driver_phone_number") ||
       !incomingData.contains("auto_model") ||
       !incomingData.contains("auto_marka") ||
       !incomingData.contains("auto_number") ||
       !incomingData.contains("auto_color") ||
       !incomingData.contains("franchising_name") ||
       !incomingData.contains("auto_id") ||
       !incomingData.contains("url_photo_driver") ||
       !incomingData.contains("images"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto editImages = incomingData.value("images").toList();
    if(editImages.count() == 0)
    {
        qDebug() << __FUNCTION__ << "error: files not found";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto idRow = selectOldIdRow(incomingData.value("driver_id").toULongLong());
    if (idRow == 0)
        return QSharedPointer<network::Response>();

    auto currentImages = selectOldPhotos(idRow);
    if(currentImages.count() == 0)
        return QSharedPointer<network::Response>();

    for(auto itEdit = editImages.begin(); itEdit != editImages.end(); itEdit++)
    {
        const auto editMap = (*itEdit).toMap();
        const auto itEditPos = editMap.find("pos");
        const auto itEditUrl = editMap.find("url");
        if(itEditPos == editMap.end() ||
           itEditUrl == editMap.end())
            continue;

        for(auto itCurrent = currentImages.begin(); itCurrent != currentImages.end(); itCurrent++)
        {
            auto currentMap = (*itCurrent).toMap();
            const auto itCurrentPos = currentMap.find("pos");
            const auto itCurrentUrl = currentMap.find("url");
            if(itCurrentPos == currentMap.end() ||
               itCurrentUrl == currentMap.end())
                continue;
            if(itEditPos.value() != itCurrentPos.value())
                continue;

            currentMap["url"] = itEditUrl.value();
            *itCurrent = currentMap;
        }
    }

    const auto idNewRow = insertNewRow(incomingData);
    if (idNewRow == 0)
        return QSharedPointer<network::Response>();

    if(!insertPhotos(idNewRow, currentImages))
        return QSharedPointer<network::Response>();

    QVariantMap head;
    QVariantMap body;
    QVariantMap result;

    head["type"] = signature();
    body["status"] = 1;

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}


quint64 EditPhotos::selectOldIdRow(const quint64 id_driver)
{
    const auto wraper = database::DBManager::instance().getDBWraper();

    QString selectIdQueryStr = QString(
                "SELECT DISTINCT ON(id_driver) id_driver, id, status, "
                "to_char(info_photos.date_create, 'YYYY-MM-DD HH24:MI:SS.MS') AS date_create "
                "FROM public.info_photos "
                "WHERE id_driver = :id_driver "
                "ORDER BY id_driver, info_photos.date_create DESC"
                );

    auto selectIdQuery = wraper->query();
    selectIdQuery.prepare(selectIdQueryStr);
    selectIdQuery.bindValue(":id_driver", id_driver);
    auto selectIdQueryResult = wraper->execQuery(selectIdQuery);
    if (!selectIdQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectIdQuery.lastError().text());
        //Q_ASSERT(false);
        return 0;
    }

    auto resultList = database::DBHelpers::queryToVariant(selectIdQuery);
    if(resultList.count() == 0)
    {
        qDebug() << __FUNCTION__ << "error: records not found";
        //Q_ASSERT(false);
        return 0;
    }
    auto result = resultList.value(0).toMap();
    if(result.count() == 0)
    {
        qDebug() << __FUNCTION__ << "error: records not found";
        //Q_ASSERT(false);
        return 0;
    }

    const auto itIdRow = result.find("id");
    const auto itStatus = result.find("status");
    if(itIdRow == result.end() ||
       itStatus == result.end() ||
       itStatus.value().toInt() == 1 ||
       itStatus.value().toInt() == 3 ||
       itIdRow.value().toULongLong() == 0)
    {
        qDebug() << __FUNCTION__ << "error: record not found";
        //Q_ASSERT(false);
        return 0;
    }

    return itIdRow.value().toULongLong();
}

QList<QVariant> EditPhotos::selectOldPhotos(const quint64 id_row)
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    QString selectOldImagesQueryStr = QString(
        "SELECT "
        "url, "
        "pos "
        "FROM public.images "
        "WHERE id_info_photos=:id_info_photos "
        );

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectOldImagesQuery = wraper->query();
    selectOldImagesQuery.prepare(selectOldImagesQueryStr);
    selectOldImagesQuery.bindValue(":id_info_photos", id_row);
    auto selectOldImagesQueryResult = wraper->execQuery(selectOldImagesQuery);
    if (!selectOldImagesQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectOldImagesQuery.lastError().text());
        //Q_ASSERT(false);
        return QList<QVariant>();
    }

    return database::DBHelpers::queryToVariant(selectOldImagesQuery);
}

quint64 EditPhotos::insertNewRow(const QMap<QString, QVariant>& incomingData)
{
    const auto wraper = database::DBManager::instance().getDBWraper();

    QString setPhotosQueryStr = QString(
        "INSERT INTO public.info_photos "
        "("
            "id_driver, "
            "driver_name, "
            "driver_number, "
            "auto_id, "
            "auto_model, "
            "auto_marka, "
            "auto_number, "
            "auto_color, "
            "franchising_name, "
            "date_create, "
            "url_photo_driver, "
            "auto_type, "
            "option_branding, "
            "option_terminal, "
            "option_chair, "
            "option_chair_cradle, "
            "option_booster, "
            "option_airport, "
            "option_checker"
        ") "
        "VALUES"
        "("
            ":id_driver, "
            ":driver_name, "
            ":driver_number, "
            ":auto_id, "
            ":auto_model, "
            ":auto_marka, "
            ":auto_number, "
            ":auto_color, "
            ":franchising_name, "
            "localtimestamp, "
            ":url_photo_driver, "
            ":auto_type, "
            ":option_branding, "
            ":option_terminal, "
            ":option_chair, "
            ":option_chair_cradle, "
            ":option_booster, "
            ":option_airport, "
            ":option_checker"
        ")"
        );

    auto setPhotosQuery = wraper->query();
    setPhotosQuery.prepare(setPhotosQueryStr);
    setPhotosQuery.bindValue(":id_driver", incomingData.value("driver_id"));
    setPhotosQuery.bindValue(":driver_name", incomingData.value("driver_full_name"));
    setPhotosQuery.bindValue(":driver_number", incomingData.value("driver_phone_number"));
    setPhotosQuery.bindValue(":auto_id", incomingData.value("auto_id"));
    setPhotosQuery.bindValue(":auto_model", incomingData.value("auto_model"));
    setPhotosQuery.bindValue(":auto_marka", incomingData.value("auto_marka"));
    setPhotosQuery.bindValue(":auto_number", incomingData.value("auto_number"));
    setPhotosQuery.bindValue(":auto_color", incomingData.value("auto_color"));
    setPhotosQuery.bindValue(":franchising_name", incomingData.value("franchising_name"));
    setPhotosQuery.bindValue(":url_photo_driver", incomingData.value("url_photo_driver"));

    auto autoType = incomingData.value("auto_type").toString();
    if (autoType.isEmpty())
        autoType = QString::number(0);
    auto branding = incomingData.value("option_branding").toString();
    if (branding.isEmpty())
        branding = QString::number(0);
    auto terminal = incomingData.value("option_terminal").toString();
    if (terminal.isEmpty())
        terminal = QString::number(0);
    auto chair = incomingData.value("option_chair").toString();
    if (chair.isEmpty())
        chair = QString::number(0);
    auto chairCradle = incomingData.value("option_chair_cradle").toString();
    if (chairCradle.isEmpty())
        chairCradle = QString::number(0);
    auto booster = incomingData.value("option_booster").toString();
    if (booster.isEmpty())
        booster = QString::number(0);
    auto airport = incomingData.value("option_airport").toString();
    if (airport.isEmpty())
        airport = QString::number(0);
    auto checker = incomingData.value("auto_checker").toString();
    if (checker.isEmpty())
        checker = QString::number(0);

    setPhotosQuery.bindValue(":auto_type", autoType);
    setPhotosQuery.bindValue(":option_branding", branding);
    setPhotosQuery.bindValue(":option_terminal", terminal);
    setPhotosQuery.bindValue(":option_chair", chair);
    setPhotosQuery.bindValue(":option_chair_cradle", chairCradle);
    setPhotosQuery.bindValue(":option_booster", booster);
    setPhotosQuery.bindValue(":option_airport", airport);
    setPhotosQuery.bindValue(":option_checker", checker);

    auto setPhotosQueryResult = wraper->execQuery(setPhotosQuery);
    if (!setPhotosQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(setPhotosQuery.lastError().text());
        //Q_ASSERT(false);
        return 0;
    }

    const QString selectIdQueryStr = QString(
        "SELECT DISTINCT ON(id_driver) id_driver, id, status, "
        "to_char(info_photos.date_create, 'YYYY-MM-DD HH24:MI:SS.MS') AS date_create "
        "FROM public.info_photos "
        "WHERE id_driver = :id_driver AND status = '0' "
        "ORDER BY id_driver, info_photos.date_create DESC"
        );

    auto selectIdQuery = wraper->query();
    selectIdQuery.prepare(selectIdQueryStr);
    selectIdQuery.bindValue(":id_driver", incomingData.value("driver_id"));
    auto selectIdQueryResult = wraper->execQuery(selectIdQuery);
    if (!selectIdQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectIdQuery.lastError().text());
        //Q_ASSERT(false);
        return 0;
    }

    auto resultList = database::DBHelpers::queryToVariant(selectIdQuery);
    if(resultList.count() == 0)
    {
        qDebug() << __FUNCTION__ << "error: records not found";
        //Q_ASSERT(false);
        return 0;
    }
    auto result = resultList.value(0).toMap();
    if(result.count() == 0)
    {
        qDebug() << __FUNCTION__ << "error: records not found";
        //Q_ASSERT(false);
        return 0;
    }

    const auto itIdRow = result.find("id");
    if(itIdRow == result.end())
    {
        qDebug() << __FUNCTION__ << "error: records not found";
        //Q_ASSERT(false);
        return 0;
    }

    return itIdRow.value().toULongLong();
}

bool EditPhotos::insertPhotos(const quint64 idRow, const QList<QVariant> & images)
{
    const auto wraper = database::DBManager::instance().getDBWraper();

    for(auto image : images)
    {
        const auto map = image.toMap();
        const auto itUrl = map.find("url");
        const auto itPos = map.find("pos");
        if(itUrl == map.end() ||
           itPos == map.end())
        {
            qDebug() << __FUNCTION__ << "error: field not found";
            //Q_ASSERT(false);
            return false;
        }

        QString insertPhotosQueryStr = QString(
            "INSERT INTO public.images "
            "(id_info_photos, url, pos) "
            "VALUES(:id_info_photos, :url, :pos)"
            );

        auto insertPhotosQuery = wraper->query();
        insertPhotosQuery.prepare(insertPhotosQueryStr);
        insertPhotosQuery.bindValue(":id_info_photos", idRow);
        insertPhotosQuery.bindValue(":url", itUrl.value());
        insertPhotosQuery.bindValue(":pos", itPos.value());

        auto insertPhotosQueryResult = wraper->execQuery(insertPhotosQuery);
        if (!insertPhotosQueryResult)
        {
            // TODO: db_error
            qDebug() << __FUNCTION__ << "error:" << qPrintable(insertPhotosQuery.lastError().text());
            //Q_ASSERT(false);
            return false;
        }
    }

    return true;
}

#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class SelectTemplate :
            public core::Command,
            public core::CommandCreator<SelectTemplate>
    {
        friend class QSharedPointer<SelectTemplate>;

        SelectTemplate(const Context& newContext);

    public:
        QSharedPointer<network::Response> exec() override;
    };

}

#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class EditPos :
            public core::Command,
            public core::CommandCreator<EditPos>
    {
        friend class QSharedPointer<EditPos>;

        EditPos(const Context& newContext);
        bool updatePos(const QVariantMap & incomingData);


    public:
        QSharedPointer<network::Response> exec() override;
    };

}

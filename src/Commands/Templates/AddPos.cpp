#include "Common.h"
#include "AddPos.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(photo_validator_server::AddPos, "add_pos")


using namespace photo_validator_server;

AddPos::AddPos(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> AddPos::exec()
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    auto incomingData = _context._packet.body().toMap();

    if(!incomingData.contains("images"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto images = incomingData.value("images").toList();
    if(images.count() == 0)
    {
        qDebug() << __FUNCTION__ << "error: files not found";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    if(!insertPos(images))
        return QSharedPointer<network::Response>();

    QVariantMap head;
    QVariantMap body;
    QVariantMap result;

    head["type"] = signature();
    body["status"] = 1;

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

bool AddPos::insertPos(const QList<QVariant> & images)
{
    const auto wraper = database::DBManager::instance().getDBWraper();

    for(auto image : images)
    {
        const auto map = image.toMap();
        const auto itUrl = map.find("url");
        const auto itComment = map.find("comment");
        if(itUrl == map.end() ||
           itComment == map.end())
        {
            qDebug() << __FUNCTION__ << "error: field not found";
            //Q_ASSERT(false);
            return false;
        }

        QString insertPosQueryStr = QString(
            "INSERT INTO public.template_images "
            "(url, comment) "
            "VALUES(:url, :comment)"
            );

        auto insertPosQuery = wraper->query();
        insertPosQuery.prepare(insertPosQueryStr);
        insertPosQuery.bindValue(":url", itUrl.value());
        insertPosQuery.bindValue(":comment", itComment.value());

        auto insertPosQueryResult = wraper->execQuery(insertPosQuery);
        if (!insertPosQueryResult)
        {
            // TODO: db_error
            qDebug() << __FUNCTION__ << "error:" << qPrintable(insertPosQuery.lastError().text());
            //Q_ASSERT(false);
            return false;
        }
    }

    return true;
}

#include "Common.h"
#include "SelectTemplate.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(photo_validator_server::SelectTemplate, "select_template")


using namespace photo_validator_server;

SelectTemplate::SelectTemplate(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> SelectTemplate::exec()
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    QString selectTemplateQueryStr = QString(
        "SELECT * "
        "FROM public.template_images "
        "ORDER BY pos"
        );

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectTemplateQuery = wraper->query();

    selectTemplateQuery.prepare(selectTemplateQueryStr);
    auto selectTemplateQueryResult = wraper->execQuery(selectTemplateQuery);
    if (!selectTemplateQueryResult)
    {
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectTemplateQuery.lastError().text());
        return network::ResponseShp();
    }
    auto templateImages = database::DBHelpers::queryToVariant(selectTemplateQuery);

    QString selectHintsQueryStr = QString(
        "SELECT * "
        "FROM public.template_hints "
        "ORDER BY id"
        );

    auto selectHintsQuery = wraper->query();

    selectHintsQuery.prepare(selectHintsQueryStr);
    if (!selectHintsQuery.exec())
    {
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectHintsQuery.lastError().text());
        return network::ResponseShp();
    }
    auto templateHints = database::DBHelpers::queryToVariant(selectHintsQuery);

    QVariantMap head;
    QVariantMap body;
    QVariantMap result;

    head["type"] = signature();
    body["status"] = 1;
    body["template"] = templateImages;
    body["template_hints"] = templateHints;

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(result));

    return network::ResponseShp();
}

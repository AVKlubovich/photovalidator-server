#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class AddPos :
            public core::Command,
            public core::CommandCreator<AddPos>
    {
        friend class QSharedPointer<AddPos>;

        AddPos(const Context& newContext);
        bool insertPos(const QList<QVariant> & images);


    public:
        QSharedPointer<network::Response> exec() override;
    };

}

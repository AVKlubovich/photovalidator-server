#include "Common.h"
#include "EditPos.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(photo_validator_server::EditPos, "edit_pos")


using namespace photo_validator_server;

EditPos::EditPos(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> EditPos::exec()
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    auto incomingData = _context._packet.body().toMap();

    if (!incomingData.contains("url") ||
        !incomingData.contains("pos") ||
        !incomingData.contains("comment"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    if(!updatePos(incomingData))
        return QSharedPointer<network::Response>();

    QVariantMap head;
    QVariantMap body;
    QVariantMap result;

    head["type"] = signature();
    body["status"] = 1;

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

bool EditPos::updatePos(const QVariantMap & incomingData)
{
    const auto wraper = database::DBManager::instance().getDBWraper();

    QString updatePosQueryStr = QString(
        "UPDATE public.template_images SET "
        "url=:url, comment=:comment "
        "WHERE pos=:pos"
        );

    auto updatePosQuery = wraper->query();
    updatePosQuery.prepare(updatePosQueryStr);
    updatePosQuery.bindValue(":url", incomingData.value("url"));
    updatePosQuery.bindValue(":comment", incomingData.value("comment"));
    updatePosQuery.bindValue(":pos", incomingData.value("pos"));

    auto updatePosQueryResult = wraper->execQuery(updatePosQuery);
    if (!updatePosQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(updatePosQuery.lastError().text());
        //Q_ASSERT(false);
        return false;
    }

    return true;
}

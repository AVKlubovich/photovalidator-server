#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class DeletePos :
            public core::Command,
            public core::CommandCreator<DeletePos>
    {
        friend class QSharedPointer<DeletePos>;

        DeletePos(const Context& newContext);
        bool deletePos(const QVariantMap & incomingData);


    public:
        QSharedPointer<network::Response> exec() override;
    };

}

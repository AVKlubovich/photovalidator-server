#include "Common.h"
#include "DeletePos.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(photo_validator_server::DeletePos, "delete_pos")


using namespace photo_validator_server;

DeletePos::DeletePos(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> DeletePos::exec()
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    auto incomingData = _context._packet.body().toMap();

    if(!incomingData.contains("pos"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    if(!deletePos(incomingData))
        return QSharedPointer<network::Response>();

    QVariantMap head;
    QVariantMap body;
    QVariantMap result;

    head["type"] = signature();
    body["status"] = 1;

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

bool DeletePos::deletePos(const QVariantMap & incomingData)
{
    const auto wraper = database::DBManager::instance().getDBWraper();

    QString deletePosQueryStr = QString(
        "DELETE FROM public.template_images "
        "WHERE pos=:pos"
        );

    auto deletePosQuery = wraper->query();
    deletePosQuery.prepare(deletePosQueryStr);
    deletePosQuery.bindValue(":pos", incomingData.value("pos"));

    auto deletePosQueryResult = wraper->execQuery(deletePosQuery);
    if (!deletePosQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(deletePosQuery.lastError().text());
        //Q_ASSERT(false);
        return false;
    }

    return true;
}

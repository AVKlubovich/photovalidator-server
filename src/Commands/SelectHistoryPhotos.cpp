#include "Common.h"
#include "SelectHistoryPhotos.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(photo_validator_server::SelectHistoryPhotos, "select_history_photos")


using namespace photo_validator_server;

SelectHistoryPhotos::SelectHistoryPhotos(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> SelectHistoryPhotos::exec()
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    auto incomingData = _context._packet.body().toMap()["body"].toMap();

    if (!incomingData.contains("date_start") ||
       !incomingData.contains("date_end") ||
       !incomingData.contains("statuses"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    QString selectHistoryPhotosQueryStr = QString(
        "SELECT "
        "info_photos.id, "
        "info_photos.id_driver, "
        "info_photos.status, "
        "info_photos.driver_name, "
        "info_photos.driver_number, "
        "info_photos.auto_model, "
        "info_photos.auto_marka, "
        "info_photos.auto_number, "
        "info_photos.auto_color, "
        "info_photos.franchising_name, "
        "info_photos.auto_type, "
        "info_photos.option_branding, "
        "info_photos.option_terminal, "
        "info_photos.option_chair, "
        "info_photos.option_chair_cradle, "
        "info_photos.option_booster, "
        "info_photos.option_airport, "
        "info_photos.option_checker, "
        "info_photos.url_photo_driver, "
        "to_char(info_photos.date_create, 'YYYY-MM-DD HH24:MI:SS.MS') AS date_create, "
        "to_char(info_photos.date_close, 'YYYY-MM-DD HH24:MI:SS.MS') AS date_close, "
        "info_photos.failed_photos, "
        "info_photos.op_comment,"
        "info_photos.verified, "
        "NULLif (users.name, '') AS user_name "
        "FROM public.info_photos "
        "LEFT JOIN public.users "
        "ON info_photos.id_user = users.id "
        "WHERE (info_photos.date_create BETWEEN :date_start AND :date_end) "
        );

    QVariantMap sqlStatusesParameters;
    const auto sqlStatuses = statusesToSql(incomingData, sqlStatusesParameters);
    if (sqlStatuses.isEmpty())
        return QSharedPointer<network::Response>();
    else
        selectHistoryPhotosQueryStr += " AND " + sqlStatuses;

    QVariantMap sqlFilterParameters;
    const auto sqlFiler = filterToSql(incomingData, sqlFilterParameters);
    if (!sqlFiler.isEmpty())
        selectHistoryPhotosQueryStr += " AND " + sqlFiler;

    selectHistoryPhotosQueryStr += " ORDER BY info_photos.date_create DESC";

    const auto& wraper = database::DBManager::instance().getDBWraper();
    auto selectHistoryPhotosQuery = wraper->query();
    selectHistoryPhotosQuery.prepare(selectHistoryPhotosQueryStr);
    selectHistoryPhotosQuery.bindValue(":date_start", incomingData.value("date_start").toDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz"));
    selectHistoryPhotosQuery.bindValue(":date_end", incomingData.value("date_end").toDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz"));
    for (QVariantMap::Iterator it = sqlStatusesParameters.begin(); it != sqlStatusesParameters.end(); it++)
        selectHistoryPhotosQuery.bindValue(it.key(), it.value());
    for (QVariantMap::Iterator it = sqlFilterParameters.begin(); it != sqlFilterParameters.end(); it++)
        selectHistoryPhotosQuery.bindValue(it.key(), it.value());

    const auto selectHistoryPhotosResult = wraper->execQuery(selectHistoryPhotosQuery);
    if (!selectHistoryPhotosResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectHistoryPhotosQuery.lastError().text());
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto& resultList = database::DBHelpers::queryToVariant(selectHistoryPhotosQuery);
    QVariantList groupPhotos;

    if (!resultList.isEmpty())
    {
        QStringList groupIds;
        for (const auto& value : resultList)
        {
            groupIds << QString::number(value.toMap()["id"].toULongLong());
        }

        const auto& selectPhotosStr = QString(
            "SELECT id_info_photos, url, pos "
            "FROM images "
            "WHERE id_info_photos IN (%1)"
            ).arg(groupIds.join(","));
        auto selectPhotosQuery = wraper->query();
        selectPhotosQuery.prepare(selectPhotosStr);

        const auto selectPhotosResult = wraper->execQuery(selectPhotosQuery);
        if (!selectPhotosResult)
        {
            qDebug() << __FUNCTION__ << "db error:" << qPrintable(selectPhotosQuery.lastError().text());
            return QSharedPointer<network::Response>();
        }

        const auto& photosResultList = database::DBHelpers::queryToVariant(selectPhotosQuery);
        for (const auto& group : resultList)
        {
            auto groupMap = group.toMap();
            const auto id = groupMap["id"].toLongLong();

            QVariantList listPhotos;
            for (const auto& photos : photosResultList)
            {
                const auto& selectPhoto = photos.toMap();
                if (selectPhoto["id_info_photos"].toInt() == id)
                {
                    QVariantMap mapPhoto;
                    mapPhoto["url"] = selectPhoto["url"];
                    mapPhoto["pos"] = selectPhoto["pos"];
                    listPhotos.append(mapPhoto);
                }
            }

            groupMap["images"] = listPhotos;
            groupPhotos.append(groupMap);
        }
    }

    QVariantMap head;
    QVariantMap result;

    head["type"] = signature();

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(groupPhotos);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

const QString SelectHistoryPhotos::statusesToSql(const QMap<QString, QVariant> & incoming, QVariantMap & outParameters)
{
    const auto statuses = incoming.value("statuses");
    const auto listStatus = statuses.toList();

    if (listStatus.count() == 0)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        //Q_ASSERT(false);
        return "";
    }

    QString query("(info_photos.status IN (");
    for (int i = 0; i < listStatus.count(); i++)
    {
        QString key = ":status" + QString::number(i+1);
        query += key + ", ";
        outParameters.insert(key, listStatus.value(i));
    }
    query = query.mid(0, query.length() - 2) + "))";

    return query;
}

const QString SelectHistoryPhotos::filterToSql(const QMap<QString, QVariant> & incoming, QVariantMap &  outParameters)
{
    if (!incoming.contains("filter_name") ||
       !incoming.contains("filter_value"))
        return "";

    auto filter_name = incoming.value("filter_name").toString();
    const auto enum_filter = stringToEnum<FilterNames::Enum>(filter_name);

    switch (enum_filter) {
    case FilterNames::driver_id:
        filter_name = "id_driver";
        break;
    case FilterNames::driver_name:
    case FilterNames::driver_number:
    case FilterNames::auto_number:
    case FilterNames::id_user:
        break;
    default:
        return "";
    }

    outParameters.insert(":" + filter_name, "%" + incoming.value("filter_value").toString() + "%");

    if (enum_filter == FilterNames::driver_id || enum_filter == FilterNames::id_user)
        return "(info_photos." + filter_name + "::bigint::text LIKE :" + filter_name + ")";

    return "(info_photos." + filter_name + " LIKE :" + filter_name + ")";
}

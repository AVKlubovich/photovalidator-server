#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class SelectNewPhotos :
            public core::Command,
            public core::CommandCreator<SelectNewPhotos>
    {
        friend class QSharedPointer<SelectNewPhotos>;

        SelectNewPhotos(const Context& newContext);

    public:
        QSharedPointer<network::Response> exec() override;
    };

}

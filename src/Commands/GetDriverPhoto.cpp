#include "Common.h"
#include "GetDriverPhoto.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "web-exchange/WebRequestManager.h"
#include "web-exchange/WebRequest.h"

#include "utils/Settings/SettingsFactory.h"
#include "utils/Settings/Settings.h"

RegisterCommand(photo_validator_server::GetDriverPhoto, "get_driver_photo")


using namespace photo_validator_server;

GetDriverPhoto::GetDriverPhoto(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> GetDriverPhoto::exec()
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    const auto& incomingData = _context._packet.body().toMap()["body"].toMap();

    if(!incomingData.contains("id_driver"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto driverId = incomingData["id_driver"].toLongLong();

    auto webManager = network::WebRequestManager::instance();

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("ApiGeneral");
    const auto urlImg = settings["UrlImg"].toString();
    webManager->setSingleUrl(urlImg);

    auto webRequest = QSharedPointer<network::WebRequest>::create("sub_qry");

    QVariantMap userData;
    userData["act"] = "show";
    const auto& driverData = QString("%1%2").arg(QString::number(driverId)).arg("drivera");
    userData["hash"] = QString(QCryptographicHash::hash(driverData.toStdString().data(),QCryptographicHash::Md5).toHex());
    userData["id"] = QString::number(driverId);
    userData["size"] = "wh";
    userData["type"] = "driver";
    userData["w"] = QString::number(1024);
    userData["h"] = QString::number(1024);

    webRequest->setArguments(userData);
    webRequest->setCallback(nullptr);
    webManager->sendRequestCurrentThread(webRequest);

    const auto& webData = webRequest->reply();
    webRequest->release();
    qDebug() << webData;

    QVariantMap head;
    QVariantMap body;
    QVariantMap result;

    head["type"] = signature();
    body["status"] = 1;

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(webData));

    return QSharedPointer<network::Response>();
}

#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class FetchNewPhotos :
            public core::Command,
            public core::CommandCreator<FetchNewPhotos>
    {
        friend class QSharedPointer<FetchNewPhotos>;

        FetchNewPhotos(const Context& newContext);

    public:
        QSharedPointer<network::Response> exec() override;
    };

}

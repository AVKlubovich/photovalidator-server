#include "Common.h"
#include "QuickSearch.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(photo_validator_server::QuickSearch, "quick_search")


using namespace photo_validator_server;

QuickSearch::QuickSearch(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> QuickSearch::exec()
{
    const auto& incomingData = _context._packet.body().toMap()["body"].toMap();

    if( (!incomingData.contains("field")) && (!incomingData.contains("data")) )
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        return QSharedPointer<network::Response>();
    }

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto query = wraper->query();

    QString nameFiled;
    QString stringQuery = QString("SELECT DISTINCT %1 FROM info_photos");
    switch (incomingData["field"].toInt())
    {
        case 1:
            nameFiled = "driver_name";
            break;
        case 2:
            nameFiled = "id_driver";
            break;
        case 3:
            nameFiled = "auto_number";
            break;
        case 4:
            nameFiled = "driver_number";
            break;
        case 5:
            nameFiled = "name, id";
            stringQuery = "SELECT %1 FROM users";
            break;
        default:
            break;
    }

    stringQuery = stringQuery.arg(nameFiled);

    query.prepare(stringQuery);

    auto selectQueryResult = wraper->execQuery(query);
    if (!selectQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(query.lastError().text());
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }
    auto searchList = database::DBHelpers::queryToVariant(query);

    QVariantMap head;
    QVariantMap result;

    head["type"] = signature();
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(searchList);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

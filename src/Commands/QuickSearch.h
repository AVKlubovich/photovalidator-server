#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class QuickSearch :
            public core::Command,
            public core::CommandCreator<QuickSearch>
    {
        friend class QSharedPointer<QuickSearch>;

        QuickSearch(const Context& newContext);

    public:
        QSharedPointer<network::Response> exec() override;
    };

}

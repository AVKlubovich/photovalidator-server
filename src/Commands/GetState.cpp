#include "Common.h"
#include "GetState.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/DefaultResponse.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(photo_validator_server::GetState, "get_state")


using namespace photo_validator_server;

GetState::GetState(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> GetState::exec()
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    auto incomingData = _context._packet.body().toMap();
    if(!incomingData.contains("driver_id"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: incoming data not found";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    QString selectStatusQueryStr = QString(
                "SELECT DISTINCT ON(id_driver) id_driver, "
                "to_char(info_photos.date_create, 'YYYY-MM-DD HH24:MI:SS.MS') AS date_create, "
                "status, failed_photos "
                "FROM public.info_photos "
                "WHERE id_driver = :id_driver "
                "ORDER BY id_driver, info_photos.date_create DESC"
                );

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectStatusQuery = wraper->query();
    selectStatusQuery.prepare(selectStatusQueryStr);
    selectStatusQuery.bindValue(":id_driver", incomingData.value("driver_id").toULongLong());
    auto selectStatusQueryResult = wraper->execQuery(selectStatusQuery);
    if (!selectStatusQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectStatusQuery.lastError().text());
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    QVariantMap body;

    auto selectStatusQueryList = database::DBHelpers::queryToVariant(selectStatusQuery);
    if (selectStatusQueryList.count() == 0)
    {
        body["car_state"] = enumToString<StateCar::Enum>(StateCar::state_write_photos);
        setTemplate(QStringList(), body);
    }
    else
    {
        const auto & map = selectStatusQueryList.first().toMap();
        if (map.count() == 0)
        {
            body["car_state"] = enumToString<StateCar::Enum>(StateCar::state_write_photos);
            setTemplate(QStringList(), body);
        }
        else
        {
            const auto currentDateTime = getDBDateTime();
            const auto state = static_cast<StateRecord::Enum>(map.value("status").toInt());
            const auto date_create = map.value("date_create").toDateTime();
            switch (state) {
            case StateRecord::state_new:
            {
                body["car_state"] = enumToString<StateCar::Enum>(StateCar::state_wait);
                setTemplate(QStringList(), body);
            }
                break;
            case StateRecord::state_allowed:
            case StateRecord::state_auto_allowed:
            {
                if(date_create > currentDateTime.addDays(-1))
                    body["car_state"] = enumToString<StateCar::Enum>(StateCar::state_allowed);
                else
                    body["car_state"] = enumToString<StateCar::Enum>(StateCar::state_write_photos);
                setTemplate(QStringList(), body);
            }
                break;
            case StateRecord::state_query:
            {
                if(date_create > currentDateTime.addSecs(-60*60*2))
                {
                    body["car_state"] = enumToString<StateCar::Enum>(StateCar::state_query);
                    auto list = map.value("failed_photos").toString().split(",");
                    auto failed_photos = list;
                    for(int i = 0; i < failed_photos.count(); i++)
                        failed_photos[i] = "pos" + failed_photos[i];
                    body["photos"] = failed_photos;
                    setTemplate(list, body);
                }
                else
                {
                    body["car_state"] = enumToString<StateCar::Enum>(StateCar::state_write_photos);
                    setTemplate(QStringList(), body);
                }
            }
                break;
            case StateRecord::state_block:
            {
                body["car_state"] = enumToString<StateCar::Enum>(StateCar::state_block);
                setTemplate(map.value("failed_photos").toString().split(","), body);
            }
                break;
            default:
            {
                // TODO: db_error
                qDebug() << __FUNCTION__ << "error: state incorrect value";
                body["car_state"] = enumToString<StateCar::Enum>(StateCar::state_write_photos);
                setTemplate(QStringList(), body);
            }
            }
        }
    }

    QVariantMap head;
    QVariantMap result;

    head["type"] = signature();
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

void GetState::setTemplate(const QStringList & pos, QVariantMap & body)
{
    QString selectTemplateQueryStr;
    if(pos.count() == 0)
    {
        selectTemplateQueryStr = QString(
        "SELECT * "
        "FROM public.template_images");
    }
    else
    {
        selectTemplateQueryStr = QString(
            "SELECT * "
            "FROM public.template_images "
            "WHERE pos IN(%1)"
            ).arg(pos.join(","));
    }

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectTemplateQuery = wraper->query();

    selectTemplateQuery.prepare(selectTemplateQueryStr);
    auto selectTemplateQueryResult = wraper->execQuery(selectTemplateQuery);
    if (!selectTemplateQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectTemplateQuery.lastError().text());
        //Q_ASSERT(false);
        return;
    }
    auto resultList = database::DBHelpers::queryToVariant(selectTemplateQuery);
    QList<QVariant> response;
    for(int i = 0; i < resultList.count(); i++)
    {
        auto map = resultList[i].toMap();
        auto it = map.find("url");
        if(it == map.end())
            continue;

        auto url = it.value().toString();
        auto index = url.lastIndexOf("/");
        if(index == -1 ||
           index+1 == url.length())
            continue;

        map["url"] = url.mid(index+1);
        response.append(map);
    }
    body["template"] = response;

    const QString& selectHintsQueryStr = QString(
        "SELECT * "
        "FROM public.template_hints "
        "ORDER BY id"
        );

    auto selectHintsQuery = wraper->query();

    selectHintsQuery.prepare(selectHintsQueryStr);
    if (!selectHintsQuery.exec())
    {
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectHintsQuery.lastError().text());
        return;
    }
    auto templateHints = database::DBHelpers::queryToVariant(selectHintsQuery);
    body["template_hints"] = templateHints;
}

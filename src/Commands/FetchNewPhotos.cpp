#include "Common.h"
#include "FetchNewPhotos.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/DefaultRequest.h"
#include "network-core/RequestsManager/DefaultResponse.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(photo_validator_server::FetchNewPhotos, "fetch_new_photos")


using namespace photo_validator_server;

FetchNewPhotos::FetchNewPhotos(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> FetchNewPhotos::exec()
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    auto incomingData = _context._packet.body().toMap()["body"].toMap();
    if (!incomingData.contains("last_fetch_date"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: incoming data not found";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    QString selectNewPhotosQueryStr = QString(
        "SELECT "
        "id, "
        "id_driver, "
        "driver_name, "
        "driver_number, "
        "auto_id, "
        "auto_model, "
        "auto_marka, "
        "auto_number, "
        "auto_color, "
        "franchising_name, "
        "to_char(info_photos.date_create, 'YYYY-MM-DD HH24:MI:SS.MS') AS date_create, "
        "auto_type, "
        "option_branding, "
        "option_terminal, "
        "option_chair, "
        "option_chair_cradle, "
        "option_booster, "
        "option_airport, "
        "option_checker, "
        "url_photo_driver,"
        "verified "
        "FROM public.info_photos "
        "WHERE info_photos.date_create > :date_create AND status = 0"
        );

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectNewPhotosQuery = wraper->query();
    selectNewPhotosQuery.prepare(selectNewPhotosQueryStr);
    selectNewPhotosQuery.bindValue(":date_create", incomingData.value("last_fetch_date").toDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz"));

    auto selectNewPhotosQueryResult = wraper->execQuery(selectNewPhotosQuery);
    if (!selectNewPhotosQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectNewPhotosQuery.lastError().text());
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    auto selectNewPhotosResultList = database::DBHelpers::queryToVariant(selectNewPhotosQuery);
    QList<quint64> ids;
    for(auto row : selectNewPhotosResultList)
    {
        auto map = row.toMap();
        const auto it = map.find("id");
        if(it != map.end())
            ids.append(it.value().toULongLong());
    }

    if(ids.count() > 0)
    {
        QString selectImagesQueryStr = QString(
            "SELECT "
            "id_info_photos, "
            "url, "
            "pos "
            "FROM public.images "
            "WHERE id_info_photos IN (%1)"
        );

        QStringList idParams;
        for(int i = 0; i < ids.count(); i++)
            idParams << ":id" + QString::number(i);

        selectImagesQueryStr = selectImagesQueryStr.arg(idParams.join(", "));

        auto selectImagesQuery = wraper->query();
        selectImagesQuery.prepare(selectImagesQueryStr);
        for(int i = 0; i < ids.count(); i++)
            selectImagesQuery.bindValue(":id" + QString::number(i), ids.value(i));

        auto selectImagesQueryResult = wraper->execQuery(selectImagesQuery);
        if (!selectImagesQueryResult)
        {
            // TODO: db_error
            qDebug() << __FUNCTION__ << "error:" << qPrintable(selectImagesQuery.lastError().text());
            //Q_ASSERT(false);
            return QSharedPointer<network::Response>();
        }

        auto resultList = database::DBHelpers::queryToVariant(selectImagesQuery);
        for(auto itGroup = selectNewPhotosResultList.begin(); itGroup != selectNewPhotosResultList.end(); itGroup++)
        {
            auto mapGroup = (*itGroup).toMap();
            const auto itIdGroup = mapGroup.find("id");
            if(itIdGroup == mapGroup.end())
                continue;

            for(auto itImage = resultList.begin(); itImage != resultList.end(); itImage++)
            {
                auto mapImage = (*itImage).toMap();
                const auto itIdImage = mapImage.find("id_info_photos");
                if(itIdImage == mapImage.end())
                    continue;
                if(itIdImage.value().toULongLong() == itIdGroup.value().toULongLong())
                {
                    auto itImages = mapGroup.find("images");
                    QList<QVariant> listImages;
                    if(itImages != mapGroup.end())
                        listImages = itImages.value().toList();
                    listImages.append(mapImage);
                    mapGroup["images"] = listImages;
                }
            }
            (*itGroup) = mapGroup;
        }
    }

    QString selectOtherPhotosStr = QString(
        "SELECT id "
        "FROM public.info_photos "
        "WHERE date_close >= :date_close AND NOT status = 0"
        );

    auto selectOtherPhotosQuery = wraper->query();
    selectOtherPhotosQuery.prepare(selectOtherPhotosStr);
    selectOtherPhotosQuery.bindValue(":date_close", incomingData.value("last_fetch_date").toDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz"));

    auto selectOtherPhotosResult = wraper->execQuery(selectOtherPhotosQuery);
    if (!selectOtherPhotosResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectOtherPhotosQuery.lastError().text());
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    auto selectOtherPhotosResultList = database::DBHelpers::queryToVariant(selectOtherPhotosQuery);

    auto& response = _context._responce;
    response->setHeaders(_context._packet.headers());

    QVariantMap head;
    QVariantMap body;
    QVariantMap result;

    body["new"] = QVariant::fromValue(selectNewPhotosResultList);
    body["other"] = QVariant::fromValue(selectOtherPhotosResultList);
    body["last_fetch_date"] = getDBDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");

    head["type"] = signature();
    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    response->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

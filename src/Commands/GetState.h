#pragma once

#include "server-core/Commands/UserCommand.h"
#include "permissions/PermissionManager.h"


namespace photo_validator_server
{
    class StateRecord :
        public QObject
    {
        Q_OBJECT

    public:
        enum Enum
        {
            state_new = 0,
            state_allowed = 1,
            state_query = 2,
            state_auto_allowed = 3,
            state_block = 4
        };
        Q_ENUM(Enum)
    };

    class StateCar :
        public QObject
    {
        Q_OBJECT

    public:
        enum Enum
        {
            state_allowed,
            state_query,
            state_wait,
            state_write_photos,
            state_block
        };
        Q_ENUM(Enum)
    };

    class GetState :
            public core::Command,
            public core::CommandCreator<GetState>
    {
        friend class QSharedPointer<GetState>;

        GetState(const Context& newContext);
        void setTemplate(const QStringList & pos, QVariantMap & body);

    public:
        QSharedPointer<network::Response> exec() override;
    };

}

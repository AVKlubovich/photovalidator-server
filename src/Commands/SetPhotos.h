#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class SetPhotos :
            public core::Command,
            public core::CommandCreator<SetPhotos>
    {
        friend class QSharedPointer<SetPhotos>;

        SetPhotos(const Context& newContext);

    public:
        QSharedPointer<network::Response> exec() override;

    private:
        quint64 insertInfo(const QVariantMap& incoming);
        bool insertPhotos(const quint64 idRow, const QList<QVariant>& images);

        QPair<quint8, qreal> validateDriverPhoto(const QString& driverPhotoUrlOrig, const QString& driverPhotoUrl);
        QString downloadPhoto(const QString& url);
        void deletePhoto(const QString& fileName);
        QHttpMultiPart* getMultiPart(const QString& photo1, const QString photo2);
    };

}

#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{
    class FilterNames :
            public QObject
    {
        Q_OBJECT

    public:
        enum Enum
        {
            driver_name,
            driver_id,
            auto_number,
            driver_number,
            id_user,
        };
        Q_ENUM(Enum)
    };


    class SelectHistoryPhotos :
            public core::Command,
            public core::CommandCreator<SelectHistoryPhotos>
    {
        friend class QSharedPointer<SelectHistoryPhotos>;

        SelectHistoryPhotos(const Context& newContext);

    public:
        QSharedPointer<network::Response> exec() override;

    private:
        const QString statusesToSql(const QMap<QString, QVariant> & incoming, QVariantMap & outParameters);
        const QString filterToSql(const QMap<QString, QVariant> & incoming, QVariantMap &  outParameters);
    };

}

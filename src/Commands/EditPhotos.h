#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class EditPhotos :
            public core::Command,
            public core::CommandCreator<EditPhotos>
    {
        friend class QSharedPointer<EditPhotos>;

        EditPhotos(const Context& newContext);

    public:
        QSharedPointer<network::Response> exec() override;

    private:
        quint64 selectOldIdRow(const quint64 id_driver);
        QList<QVariant> selectOldPhotos(const quint64 id_row);
        quint64 insertNewRow(const QMap<QString, QVariant>& incomingData);
        bool insertPhotos(const quint64 idRow, const QList<QVariant> & images);

    private:
        const quint8 PHOTOS_COUNT = 6;
    };

}

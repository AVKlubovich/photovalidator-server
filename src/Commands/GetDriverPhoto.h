#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    class GetDriverPhoto :
            public core::Command,
            public core::CommandCreator<GetDriverPhoto>
    {
        friend class QSharedPointer<GetDriverPhoto>;

        GetDriverPhoto(const Context& newContext);

    public:
        QSharedPointer<network::Response> exec() override;
    };

}

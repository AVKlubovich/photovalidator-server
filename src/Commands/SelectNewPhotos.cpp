#include "Common.h"
#include "SelectNewPhotos.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

RegisterCommand(photo_validator_server::SelectNewPhotos, "select_new_photos")


using namespace photo_validator_server;

SelectNewPhotos::SelectNewPhotos(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> SelectNewPhotos::exec()
{
    qDebug() << __FUNCTION__ << " was runned" << QDateTime::currentDateTime();

    QString selectNewPhotosQueryStr = QString(
        "SELECT "
        "id, "
        "id_driver, "
        "driver_name, "
        "driver_number, "
        "auto_id, "
        "auto_model, "
        "auto_marka, "
        "auto_number, "
        "auto_color, "
        "franchising_name, "
        "to_char(info_photos.date_create, 'YYYY-MM-DD HH24:MI:SS.MS') AS date_create, "
        "auto_type, "
        "option_branding, "
        "option_terminal, "
        "option_chair, "
        "option_chair_cradle, "
        "option_booster, "
        "option_airport, "
        "option_checker, "
        "url_photo_driver, "
        "verified "
        "FROM public.info_photos "
        "WHERE status = '0' "
        "ORDER BY info_photos.date_create"
        );

    const auto wraper = database::DBManager::instance().getDBWraper();
    auto selectNewPhotosQuery = wraper->query();

    selectNewPhotosQuery.prepare(selectNewPhotosQueryStr);
    auto selectNewPhotosQueryResult = wraper->execQuery(selectNewPhotosQuery);
    if (!selectNewPhotosQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectNewPhotosQuery.lastError().text());
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }
    auto infoPhotosResultList = database::DBHelpers::queryToVariant(selectNewPhotosQuery);

    const auto stringSqlQuery = QString(
        "SELECT id_info_photos, url, pos "
        "FROM public.images "
        "WHERE id_info_photos IN (SELECT id FROM public.info_photos WHERE status = '0' ORDER BY info_photos.date_create) "
        "ORDER BY pos");
    selectNewPhotosQuery.prepare(stringSqlQuery);
    selectNewPhotosQueryResult = wraper->execQuery(selectNewPhotosQuery);
    if (!selectNewPhotosQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectNewPhotosQuery.lastError().text());
        return QSharedPointer<network::Response>();
    }
    auto photosResultList = database::DBHelpers::queryToVariant(selectNewPhotosQuery);

    QVariantList selectList;
    for (auto infoPhotos : infoPhotosResultList)
    {
        QVariantMap selectMap = infoPhotos.toMap();
        const auto id = selectMap["id"].toInt();

        QVariantList listPhotos;
        for (auto photos : photosResultList)
        {
            QVariantMap selectPhoto = photos.toMap();
            if (selectPhoto["id_info_photos"].toInt() == id)
            {
                QVariantMap mapPhoto;
                mapPhoto["url"] = selectPhoto["url"];
                mapPhoto["pos"] = selectPhoto["pos"];
                listPhotos.append(mapPhoto);
            }
        }

        selectMap["images"] = listPhotos;
        selectList.append(selectMap);
    }

    QVariantMap head;
    QVariantMap body;
    QVariantMap result;

    head["type"] = signature();
    body["last_fetch_date"] = getDBDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz");
    selectList.append(body);

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(selectList);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

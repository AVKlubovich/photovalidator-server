#include "Common.h"
#include "SetPhotos.h"

#include "server-core/Commands/CommandFactory.h"
#include "server-core/Responce/Responce.h"

#include "network-core/RequestsManager/Users/ResponseLogin.h"
#include "network-core/RequestsManager/Users/RequestLogin.h"

#include "database/DBHelpers.h"
#include "database/DBManager.h"
#include "database/DBWraper.h"

#include "utils/Settings/SettingsFactory.h"
#include "utils/Settings/Settings.h"

#include "web-exchange/WebRequest.h"
#include "web-exchange/WebRequestManager.h"

RegisterCommand(photo_validator_server::SetPhotos, "set_photos")


using namespace photo_validator_server;

SetPhotos::SetPhotos(const Context& newContext)
    : Command(newContext)
{
}

QSharedPointer<network::Response> SetPhotos::exec()
{
    qDebug() << __FUNCTION__ << "was runned" << QDateTime::currentDateTime();

    const auto& incomingData = _context._packet.body().toMap();

    if (!incomingData.contains("driver_id") ||
        !incomingData.contains("images") ||
        !incomingData.contains("driver_full_name") ||
        !incomingData.contains("driver_phone_number") ||
        !incomingData.contains("auto_id") ||
        !incomingData.contains("auto_model") ||
        !incomingData.contains("auto_marka") ||
        !incomingData.contains("auto_number") ||
        !incomingData.contains("auto_color") ||
        !incomingData.contains("url_photo_driver") ||
        !incomingData.contains("franchising_name"))
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error: field not sended";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto images = incomingData.value("images").toList();
    if(images.count() == 0)
    {
        qDebug() << __FUNCTION__ << "error: files not found";
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto& updateRowsQueryStr = QString(
        "UPDATE public.info_photos SET status = 3 WHERE id_driver = :id_driver AND status = 0"
        );
    const auto wraper = database::DBManager::instance().getDBWraper();
    auto updateRowsQuery = wraper->query();
    updateRowsQuery.prepare(updateRowsQueryStr);
    updateRowsQuery.bindValue(":id_driver", incomingData.value("driver_id"));
    const auto updateRowsQueryResult = wraper->execQuery(updateRowsQuery);
    if (!updateRowsQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << updateRowsQuery.lastError();
        //Q_ASSERT(false);
        return QSharedPointer<network::Response>();
    }

    const auto id = insertInfo(incomingData);
    if(id == 0)
        return QSharedPointer<network::Response>();

    if(!insertPhotos(id, images))
        return QSharedPointer<network::Response>();

    QVariantMap head;
    QVariantMap body;
    QVariantMap result;

    head["type"] = signature();
    body["status"] = 1;

    result["head"] = QVariant::fromValue(head);
    result["body"] = QVariant::fromValue(body);
    _context._responce->setBody(QVariant::fromValue(result));

    return QSharedPointer<network::Response>();
}

quint64 SetPhotos::insertInfo(const QVariantMap & incoming)
{
    const auto wraper = database::DBManager::instance().getDBWraper();

    const auto& setPhotosQueryStr = QString(
        "INSERT INTO public.info_photos "
        "("
            "id_driver, "
            "driver_name, "
            "driver_number, "
            "auto_id, "
            "auto_model, "
            "auto_marka, "
            "auto_number, "
            "auto_color, "
            "franchising_name, "
            "date_create, "
            "url_photo_driver, "
            "auto_type, "
            "option_branding, "
            "option_terminal, "
            "option_chair, "
            "option_chair_cradle, "
            "option_booster, "
            "option_airport, "
            "option_checker, "
            "verified, "
            "confidence"
        ") "
        "VALUES"
        "("
            ":id_driver, "
            ":driver_name, "
            ":driver_number, "
            ":auto_id, "
            ":auto_model, "
            ":auto_marka, "
            ":auto_number, "
            ":auto_color, "
            ":franchising_name, "
            "localtimestamp, "
            ":url_photo_driver, "
            ":auto_type, "
            ":option_branding, "
            ":option_terminal, "
            ":option_chair, "
            ":option_chair_cradle, "
            ":option_booster, "
            ":option_airport, "
            ":option_checker, "
            ":verified, "
            ":confidence"
        ")"
        );

    const auto driverId = incoming.value("driver_id").toULongLong();
    const auto& images = incoming.value("images").toList();
    const auto& driverPhotoUrl = images.last().toMap()["url"].toString();
    const auto& driverPhotoUtlOrig = incoming.value("url_photo_driver").toString();

    auto setPhotosQuery = wraper->query();
    setPhotosQuery.prepare(setPhotosQueryStr);
    setPhotosQuery.bindValue(":id_driver", driverId);
    setPhotosQuery.bindValue(":driver_name", incoming.value("driver_full_name"));
    setPhotosQuery.bindValue(":driver_number", incoming.value("driver_phone_number"));
    setPhotosQuery.bindValue(":auto_id", incoming.value("auto_id"));
    setPhotosQuery.bindValue(":auto_model", incoming.value("auto_model"));
    setPhotosQuery.bindValue(":auto_marka", incoming.value("auto_marka"));
    setPhotosQuery.bindValue(":auto_number", incoming.value("auto_number"));
    setPhotosQuery.bindValue(":auto_color", incoming.value("auto_color"));
    setPhotosQuery.bindValue(":franchising_name", incoming.value("franchising_name"));
    setPhotosQuery.bindValue(":url_photo_driver", driverPhotoUtlOrig);

    auto autoType = incoming.value("auto_type").toString();
    if (autoType.isEmpty())
        autoType = QString::number(0);
    auto branding = incoming.value("option_branding").toString();
    if (branding.isEmpty())
        branding = QString::number(0);
    auto terminal = incoming.value("option_terminal").toString();
    if (terminal.isEmpty())
        terminal = QString::number(0);
    auto chair = incoming.value("option_chair").toString();
    if (chair.isEmpty())
        chair = QString::number(0);
    auto chairCradle = incoming.value("option_chair_cradle").toString();
    if (chairCradle.isEmpty())
        chairCradle = QString::number(0);
    auto booster = incoming.value("option_booster").toString();
    if (booster.isEmpty())
        booster = QString::number(0);
    auto airport = incoming.value("option_airport").toString();
    if (airport.isEmpty())
        airport = QString::number(0);
    auto checker = incoming.value("auto_checker").toString();
    if (checker.isEmpty())
        checker = QString::number(0);

    setPhotosQuery.bindValue(":auto_type", autoType);
    setPhotosQuery.bindValue(":option_branding", branding);
    setPhotosQuery.bindValue(":option_terminal", terminal);
    setPhotosQuery.bindValue(":option_chair", chair);
    setPhotosQuery.bindValue(":option_chair_cradle", chairCradle);
    setPhotosQuery.bindValue(":option_booster", booster);
    setPhotosQuery.bindValue(":option_airport", airport);
    setPhotosQuery.bindValue(":option_checker", checker);

    const auto& verifiedPair = validateDriverPhoto(driverPhotoUrl, driverPhotoUtlOrig);
    const auto verified = verifiedPair.first;
    const auto confidence = verifiedPair.second;
    setPhotosQuery.bindValue(":verified", verified);
    setPhotosQuery.bindValue(":confidence", confidence);

    auto setPhotosQueryResult = wraper->execQuery(setPhotosQuery);
    if (!setPhotosQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(setPhotosQuery.lastError().text());
        //Q_ASSERT(false);
        return 0;
    }

    const QString& selectIdQueryStr = QString(
        "SELECT DISTINCT ON(id_driver) id_driver, id, status, "
        "to_char(info_photos.date_create, 'YYYY-MM-DD HH24:MI:SS.MS') AS date_create "
        "FROM public.info_photos "
        "WHERE id_driver = :id_driver AND status = '0' "
        "ORDER BY id_driver, info_photos.date_create DESC"
        );

    auto selectIdQuery = wraper->query();
    selectIdQuery.prepare(selectIdQueryStr);
    selectIdQuery.bindValue(":id_driver", driverId);
    auto selectIdQueryResult = wraper->execQuery(selectIdQuery);
    if (!selectIdQueryResult)
    {
        // TODO: db_error
        qDebug() << __FUNCTION__ << "error:" << qPrintable(selectIdQuery.lastError().text());
        //Q_ASSERT(false);
        return 0;
    }

    auto resultList = database::DBHelpers::queryToVariant(selectIdQuery);
    if (resultList.count() == 0)
    {
        qDebug() << __FUNCTION__ << "error: records not found";
        //Q_ASSERT(false);
        return 0;
    }
    auto result = resultList.value(0).toMap();
    if (result.count() == 0)
    {
        qDebug() << __FUNCTION__ << "error: records not found";
        //Q_ASSERT(false);
        return 0;
    }

    const auto itIdRow = result.find("id");
    if (itIdRow == result.end())
    {
        qDebug() << __FUNCTION__ << "error: records not found";
        //Q_ASSERT(false);
        return 0;
    }

    return itIdRow.value().toULongLong();
}

bool SetPhotos::insertPhotos(const quint64 idRow, const QList<QVariant>& images)
{
    const auto wraper = database::DBManager::instance().getDBWraper();

    for (const auto& image : images)
    {
        const auto map = image.toMap();
        const auto itUrl = map.find("url");
        const auto itPos = map.find("pos");
        if (itUrl == map.end() ||
            itPos == map.end())
        {
            qDebug() << __FUNCTION__ << "error: field not found";
            Q_ASSERT(false);
            return false;
        }

        const auto& insertPhotosQueryStr = QString(
            "INSERT INTO public.images "
            "(id_info_photos, url, pos) "
            "VALUES(:id_info_photos, :url, :pos)"
            );

        auto insertPhotosQuery = wraper->query();
        insertPhotosQuery.prepare(insertPhotosQueryStr);
        insertPhotosQuery.bindValue(":id_info_photos", idRow);
        insertPhotosQuery.bindValue(":url", itUrl.value());
        insertPhotosQuery.bindValue(":pos", itPos.value());

        const auto insertPhotosQueryResult = insertPhotosQuery.exec();
        if (!insertPhotosQueryResult)
        {
            qDebug() << __FUNCTION__ << "error:" << qPrintable(insertPhotosQuery.lastError().text());
            Q_ASSERT(false);
            return false;
        }
    }

    return true;
}

QPair<quint8, qreal> SetPhotos::validateDriverPhoto(const QString& driverPhotoUrlOrig, const QString& driverPhotoUrl)
{
    enum
    {
        verifiedNotOk = 0,
        verifiedOk
    };

    const auto& photo1 = downloadPhoto(driverPhotoUrlOrig);
    const auto& photo2 = downloadPhoto(driverPhotoUrl);

    auto settings = utils::SettingsFactory::instance().currentSettings();
    settings.beginGroup("FindFaceAPI");
    const auto& verifyUrl = settings["Url"].toString();

    // NOTE: replace in release
//    const auto& token = settings["OldToken"].toString();
    const auto& token = settings["Token"].toString();
    const auto& authorizationHeader = QString("Token %1").arg(token);

    QNetworkRequest verifyRequest(verifyUrl);
    QSslConfiguration config = QSslConfiguration::defaultConfiguration();
    verifyRequest.setSslConfiguration(config);

    verifyRequest.setRawHeader("Authorization", authorizationHeader.toUtf8());

    QNetworkAccessManager pManager;
    QHttpMultiPart *multiPart = getMultiPart(photo1, photo2);

    const auto& reply = pManager.post(verifyRequest, multiPart);
    multiPart->setParent(reply);

    if (!reply->isFinished() && reply->error() == QNetworkReply::NoError)
    {
        QEventLoop loop;
        loop.connect(&pManager, &QNetworkAccessManager::finished, [&loop](QNetworkReply*)
        {
            loop.quit();
        });
        QTimer::singleShot(50000, &loop, SLOT(quit()));
        loop.exec();
    }

    deletePhoto(photo1);
    deletePhoto(photo2);

    if (reply->error() == QNetworkReply::NoError)
    {
        const auto& replyData = reply->readAll();
        const auto& doc = QJsonDocument::fromJson(replyData);
        const auto& json = doc.object();
        const auto& results = json["results"].toArray();
        const auto& result = results.first().toObject();
        const auto& confidence = result["confidence"].toDouble();
        const auto verified = result["verified"].toBool();

        QPair<quint8, qreal> resultPair;
        resultPair.first = verified ? verifiedOk : verifiedNotOk;
        resultPair.second = confidence;

        return resultPair;

        /*
         * Example

        "results":
        [
            {
                "bbox1":
                {
                    "x1": 262,
                    "x2": 485,
                    "y1": 188,
                    "y2": 411
                },
                "bbox2":
                {
                    "x1": 255,
                    "x2": 523,
                    "y1": 226,
                    "y2": 493
                },
                "confidence": 0.7751679420471191,
                "verified": true
            }
        ],
        "verified": true

        */
    }
    else
    {
        qDebug() << reply->errorString();
        qDebug() << reply->error();
        qDebug() << "Was unable to get reply from server findface.pro";
    }

    QPair<quint8, qreal> resultPair;
    resultPair.first = verifiedNotOk;
    resultPair.second = 0;

    return resultPair;
}

QString SetPhotos::downloadPhoto(const QString& url)
{
    QNetworkAccessManager dManager;
    QNetworkRequest request(url);

    QNetworkReply *reply = dManager.get(request);

    QEventLoop loop;
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();

    QFile file(QString("photos\%1").arg(qrand() % 1000));
    if (file.open(QIODevice::WriteOnly))
        file.write(reply->readAll());
    else
        qDebug() << "ERROR SyKA";
    file.close();

    return file.fileName();
}

void SetPhotos::deletePhoto(const QString& fileName)
{
    QFile file(fileName);
    file.remove();
}

QHttpMultiPart* SetPhotos::getMultiPart(const QString& photo1, const QString photo2)
{
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    {
        auto settings = utils::SettingsFactory::instance().currentSettings();
        settings.beginGroup("FindFaceAPI");
        const auto& threshold = settings["Threshold"].toString();

        QHttpPart thresholdCommandPart;
        thresholdCommandPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"threshold\""));
        thresholdCommandPart.setBody(threshold.toUtf8());
        multiPart->append(thresholdCommandPart);
    }

    {
        QHttpPart imagePart;
        imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
        imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(QString("form-data; name=\"photo1\"; filename=\"%1\"").arg(photo1)));
        imagePart.setRawHeader("Content-Transfer-Encoding","binary");

        QFile *file = new QFile(photo1);
        file->open(QIODevice::ReadOnly);
        imagePart.setBodyDevice(file);
        file->setParent(multiPart);
        multiPart->append(imagePart);
    }

    {
        QHttpPart imagePart;
        imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
        imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(QString("form-data; name=\"photo2\"; filename=\"%1\"").arg(photo2)));
        imagePart.setRawHeader("Content-Transfer-Encoding","binary");

        QFile *file = new QFile(photo2);
        file->open(QIODevice::ReadOnly);
        imagePart.setBodyDevice(file);
        file->setParent(multiPart);
        multiPart->append(imagePart);
    }

    return multiPart;
}

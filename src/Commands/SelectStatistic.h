#pragma once

#include "server-core/Commands/UserCommand.h"


namespace photo_validator_server
{

    namespace engine
    {
        class PermissionManager;
        typedef QSharedPointer <PermissionManager> permissionManagerShp;
    }

    class SelectStatistic :
            public core::Command,
            public core::CommandCreator<SelectStatistic>
    {
        friend class QSharedPointer<SelectStatistic>;

        SelectStatistic(const Context& newContext);

    public:
        QSharedPointer<network::Response> exec() override;
    };

}
